console.log("Hello Pokemon World");


let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		hoenn: ['May', 'Max'],
		kanto: ['Brock', 'Misty']
	},
	talk: function(){
		console.log(this.pokemon[0] + " I choose you!");
	}
}


console.log(trainer);
console.log("Result of dot notation:");
console.log(trainer.name);

console.log("Result of square bracket notation:");
console.log(trainer['pokemon']);

console.log("Result of talk method");
trainer.talk();

let certainPokemon = {
		name: "Pikachu",
		level: 12,
		health: 16,
		attack: 12,
		tackle: function(){
			console.log(this.name + " tackled targetPokemon");
			console.log("targetPokemon's health is now reduced to");
		},
		faint: function(){
			console.log("targetPokemon fainted");
		},
};
console.log(certainPokemon);

//Pokemon Object Constructor
function pokemon(name, level, health, attack){
	//properties
	this.name = name;
	this.level = level;
	this.health = health;

	//Methods
	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);
		console.log(target.name + "'s health is now reduced to");
	};

	this.faint = function(){
		console.log(target.name + " fainted");
	};
}

	//new instances of Pokemon
	let geodude = new pokemon ("Geodude", 8, -84, 8);
	let mewtwo = new pokemon ("Mewtwo", 100, 200, 100);

	geodude.tackle(certainPokemon);
	mewto.tackle(geodude);
